#!/usr/bin/env python3

import os
import re
import stat
import json
import pickle
import shutil
import logging
import getpass
import argparse
import requests
import mechanicalsoup

from datetime import datetime
from urllib.parse import urlparse, parse_qs

logging.basicConfig()

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

COURSE_TYPES = {
    "9": "Projektgruppe",
    "11": "Kulturforum",
    "13": "sonstige",
    "7": "sonstige",
    "3": "Übung",
    "6": "Forschungsgruppe",
    "10": "sonstige",
    "5": "Colloquium",
    "12": "Veranstaltungsboard",
    "4": "Praktikum",
    "2": "Seminar",
    "1": "Vorlesung",
    "8": "Gremium",
    "99": "Studiengruppe",
    "103": "Vorlesung-Übung",
    "111": "Große Übung",
    "161": "Online-Vorlesung",
}


def escape_filename(name):
    keepcharacters = (".", "_", "(", ")", ",", ":", "-", " ", "[", "]")
    return "".join(c if c.isalnum() or c in keepcharacters else "_" for c in name)


def get_directory_tree(root):
    if not os.path.isdir(root):
        return None
    fnms = filter(os.path.isfile, (os.path.join(root, f) for f in os.listdir(root)))
    files = {os.path.basename(f): {"ts": os.path.getmtime(f)} for f in fnms}
    dirs = filter(os.path.isdir, (os.path.join(root, f) for f in os.listdir(root)))
    return {
        "name": os.path.basename(root),
        "files": files,
        "subf": [get_directory_tree(d) for d in dirs],
    }


def get_file_update_list(dup, dloc, root):
    l = []
    for f in dup["files"]:
        if dloc is None:
            l.append((dup["files"][f]["fid"], os.path.join(root, f)))
            continue
        try:
            if dup["files"][f]["ts"] > dloc["files"][f]["ts"]:
                l.append((dup["files"][f]["fid"], os.path.join(root, f)))
        except KeyError:
            l.append((dup["files"][f]["fid"], os.path.join(root, f)))

    for f in dup["subf"]:
        d = (
            next(filter(lambda s: s["name"] == f["name"], dloc["subf"]), None)
            if dloc is not None
            else None
        )
        l.extend(get_file_update_list(f, d, os.path.join(root, f["name"])))

    return l


class TUCrawl(mechanicalsoup.StatefulBrowser):
    def __init__(self, user, password=None):
        super().__init__(
            user_agent="Mozilla/5.0 (X11; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0"
        )
        self.user = user
        self.password = password
        self.user_id = None
        self.semester = None

    def api(self, path, *args, **kwargs):
        txt = self.get(
            "https://studip.tu-braunschweig.de/api.php" + path, *args, **kwargs
        ).text
        return json.loads(txt)

    def login(self):
        log.info("Loggin in...")
        try:
            with open("cookies", "rb") as f:
                cookies = pickle.load(f)
                self.set_cookiejar(requests.utils.cookiejar_from_dict(cookies))

            # nobody else should read this
            os.chmod("cookies", stat.S_IRUSR | stat.S_IWUSR)
        except FileNotFoundError:
            while True:
                if self.password is None:
                    self.password = getpass.getpass("Password: ")

                # step one: select TU Braunschweig for single sing on
                self.open(
                    "https://studip.tu-braunschweig.de/index.php?cancel_login=1&again=yes&sso=saml"
                )
                self.select_form()
                self.submit_selected()

                # step two: submit our username and password to the page we get after selecting TU BS
                login = self.select_form()
                login.set("username", self.user)
                login.set("password", self.password)
                self.submit_selected()

                # some error ? -> try again...
                error_box = self.get_current_page().find(class_="error")
                if error_box:
                    self.password = None
                    log.warning("Authentication failed: %s", error_box.text)
                else:
                    # step three: consent to sharing sign on with Stud.IP...
                    self.select_form()
                    self.submit_selected(btnName="submit")

                    # step four: no javascript -> need to press another button...
                    self.select_form()
                    self.submit_selected()

                    break
        self.user_id = self.api("/user")["user_id"]
        semesters = self.api("/semesters")["collection"]
        for semester in semesters.values():
            if semester["begin"] < datetime.timestamp(datetime.now()) < semester["end"]:
                self.semester = semester["id"]
                break
        else:
            log.error("Failed to determine current semester, exiting...")
            exit(1)

    def save_login(self):
        with open("cookies", "wb") as f:
            cookies = requests.utils.dict_from_cookiejar(self.get_cookiejar())
            pickle.dump(cookies, f)
        log.info("Saved login")

    def get_courses(self):
        """
        Return my courses like this:

        [{ course_id: "id", title: "title", ...}, ...]
        """
        courses = self.api(
            f"/user/{self.user_id}/courses", params={"semester": self.semester}
        )["collection"].values()
        return courses

    def open_course(self, cid):
        self.open(
            f"https://studip.tu-braunschweig.de/dispatch.php/course/files?cid={cid}"
        )

    def open_folder(self, did, cid):
        self.open(
            f"https://studip.tu-braunschweig.de/dispatch.php/course/files/index/{did}?cid={cid}"
        )

    def get_did(self):
        return (
            self.get_current_page()
            .find("input", attrs={"name": "parent_folder_id"})
            .attrs["value"]
        )

    def get_directory_tree(self, did, cid):
        folder_info = self.api(f"/folder/{did}")
        name = escape_filename(folder_info["name"])
        subfolder_ids = [subf["id"] for subf in folder_info["subfolders"] if subf["is_readable"]]
        return {
            "did": did,
            "name": name,
            "files": {
                escape_filename(f["name"]): {"fid": f["id"], "ts": f["chdate"]}
                for f in folder_info["file_refs"]
            },
            "subf": [self.get_directory_tree(sdid, cid) for sdid in subfolder_ids],
        }

    def download_file(self, fid, path):
        log.info("Updating: '%s'.", path)
        d = os.path.dirname(path)
        if not os.path.isdir(d):
            os.makedirs(d, exist_ok=True)
        response = self.get(
            f"https://studip.tu-braunschweig.de/sendfile.php?force_download=1&file_id={fid}",
            stream=True,
        )

        if not response.ok:
            log.warning("Failed to update: '%s'.", path)
            return

        with open(path, "wb") as dest_file:
            for block in response.iter_content(1024 * 1024):
                dest_file.write(block)

    def update_studip_course_files(self, course):
        course_dir = escape_filename(
            f"{course['title']}.{COURSE_TYPES[course['type']]}"
        )

        cid = course["course_id"]
        did = self.api(f"/course/{cid}/top_folder")["id"]
        dup = self.get_directory_tree(did, cid)
        dloc = get_directory_tree(course_dir)
        upd = get_file_update_list(dup, dloc, course_dir)

        for fid, p in upd:
            self.download_file(fid, p)

    def update_files(self):
        courses = self.get_courses()
        for c in courses:
            self.update_studip_course_files(c)

    def download_file_from_path(self, url, path):
        log.info("Updating: '%s'.", path)
        d = os.path.dirname(path)
        if not os.path.isdir(d):
            os.makedirs(d, exist_ok=True)
        with open(path, "wb") as dest_file:
            response = self.get(url, stream=True)

            if not response.ok:
                log.warning("Failed to update: '%s'.", path)
                return

            for block in response.iter_content(1024):
                dest_file.write(block)

    def get_video_info(self, vid):
        return json.loads(
            self.get(
                f"https://opencast-present.rz.tu-bs.de/search/episode.json?id={vid}"
            ).content
        )

    def get_video_title(self, vid_info):
        return vid_info["search-results"]["result"]["mediapackage"]["title"]

    def get_video_url(self, vid_info):
        t = vid_info["search-results"]["result"]["mediapackage"]["media"]["track"]
        # select the track with highest bitrate
        if isinstance(t, list):
            t = sorted(
                t, key=lambda tr: tr["video"]["bitrate"] + tr["audio"]["bitrate"]
            )[-1]
        return t["url"]

    def get_video_timestamp(self, vid_info):
        ts = vid_info["search-results"]["result"]["dcCreated"]
        return datetime.fromisoformat(ts).timestamp()

    def get_course_videos(self, cid):
        self.open(
            f"https://studip.tu-braunschweig.de/plugins.php/opencast/course?cid={cid}"
        )
        l = []
        for li in self.get_current_page().find_all("li", class_="oce_item"):
            vid = li["id"]
            vid_info = self.get_video_info(vid)
            title = self.get_video_title(vid_info)
            url = self.get_video_url(vid_info)
            suffix = url.split(".")[-1]
            ts = self.get_video_timestamp(vid_info)
            l.append(
                {"fname": escape_filename(title + "." + suffix), "url": url, "ts": ts}
            )
        return l

    def get_video_directory_tree(self, course):
        return {
            "name": escape_filename(
                f"{course['title']}.{COURSE_TYPES[course['type']]}"
            ),
            "files": {
                v["fname"]: {"fid": v["url"], "ts": v["ts"]}
                for v in self.get_course_videos(course["course_id"])
            },
            "subf": [],
        }

    def update_course_video_files(self, course):
        dup = self.get_video_directory_tree(course)
        dir_name = os.path.join("recordings", dup["name"])
        dloc = get_directory_tree(dir_name)
        upd = get_file_update_list(dup, dloc, dir_name)

        for url, p in upd:
            self.download_file_from_path(url, p)

    def update_video_files(self):
        courses = self.get_courses()
        for c in courses:
            self.update_course_video_files(c)

    def get_do_it_exercise_folder_student(self, a):
        self.open(f"https://studip.tu-braunschweig.de{a['href']}")
        title = a.find("img").attrs["title"]
        tbl = (
            self.get_current_page()
            .find("div", class_="lecturer_student_answer_body")
            .find("table")
        )
        if not tbl:
            return None
        trs = tbl.find_all("tr")
        files = {}
        for tr in trs:
            tds = tr.find_all("td")
            if len(tds) < 4:
                continue
            a = tds[1].find("a")
            url = "https://studip.tu-braunschweig.de" + a["href"]
            t = escape_filename(a.text)
            ts = datetime.strptime(tds[3].text.strip(), "%d.%m.%Y %H:%M").timestamp()
            files[t] = {"fid": url, "ts": ts}
        d = {
            "name": escape_filename(title),
            "files": files,
            "subf": [],
        }
        return d

    def get_do_it_exercise_folder(self, a):
        self.open(f"https://studip.tu-braunschweig.de{a['href']}")
        tbl = self.get_current_page().find("table", id="participants")
        if not tbl:
            return None
        links = [img.parent for img in tbl.find_all("img", class_="avatar-small")]
        title = a.text
        d = {
            "name": escape_filename(title),
            "files": {},
            "subf": [
                subf
                for a in links
                if (subf := self.get_do_it_exercise_folder_student(a))
            ],
        }
        return d

    def get_do_it_subfolders(self, cid, pattern):
        self.open(
            f"https://studip.tu-braunschweig.de/plugins.php/reloadedplugin/show?cid={cid}"
        )
        l = []
        for dv in self.get_current_page().find_all("div", class_="public_thema_title"):
            a = dv.find("a")
            title = a.text
            if not re.search(pattern, title):
                continue
            dv = dv.find_next_sibling("div")
            d = {
                "name": escape_filename(title),
                "files": {},
                "subf": [
                    subf
                    for a in dv.find_all("a")
                    if (subf := self.get_do_it_exercise_folder(a))
                ],
            }
            l.append(d)
        return l

    def get_do_it_directory_tree(self, course, pattern):
        d = {
            "name": escape_filename(
                f"{course['title']}.{COURSE_TYPES[course['type']]}"
            ),
            "files": {},
            "subf": self.get_do_it_subfolders(course["course_id"], pattern),
        }
        return d

    def update_course_do_it_files(self, course, pattern):
        dup = self.get_do_it_directory_tree(course, pattern)
        dir_name = os.path.join("do_it", dup["name"])
        dloc = get_directory_tree(dir_name)
        upd = get_file_update_list(dup, dloc, dir_name)
        for url, p in upd:
            self.download_file_from_path(url, p)

    def update_do_it_files(self, pattern):
        courses = self.get_courses()
        for c in courses:
            self.update_course_do_it_files(c, pattern)


def run():
    parser = argparse.ArgumentParser(__file__)
    parser.add_argument("User", type=str)
    parser.add_argument("-p", "--password", type=str)
    parser.add_argument("-s", "--save", action="store_true")
    parser.add_argument("-v", "--videos", action="store_true")
    parser.add_argument("-d", "--do-it", type=str, nargs="?", default=None, const=".+")

    args = parser.parse_args()
    crwl = TUCrawl(args.User, args.password)
    crwl.login()
    if args.save:
        crwl.save_login()
    if args.videos:
        crwl.update_video_files()
    if args.do_it:
        crwl.update_do_it_files(args.do_it)
    else:
        crwl.update_files()
    log.info("Done !")


if __name__ == "__main__":
    run()
